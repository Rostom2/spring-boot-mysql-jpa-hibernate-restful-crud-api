package com.example.easynotes.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;

import java.io.Serializable;
import java.util.Date;

@Entity 
@Table(name = "notes")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"},
		allowGetters = true)

public class Note implements Serializable{
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String title;
    
    @NotBlank
    private String content;
    
    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt;

	public Object getTitle() {
		// TODO Auto-generated method stub
		return title;
	}

	public Object getContent() {
		// TODO Auto-generated method stub
		return content;
	}

	public void setTitle(Object title) {
		// TODO Auto-generated method stub
		this.title = (@NotBlank String) title;
	}

	public void setContent(Object content) {
		// TODO Auto-generated method stub
		this.content = (@NotBlank String) content;
	}

	

}
